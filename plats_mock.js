"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.plats = void 0;
exports.plats = [
    {
        entrees: [
            'gaspacho de tomate andalou',
            'ceviche de Maigre leche del tigre',
            'tomate coeur de bœuf burrata',
            'tartare de boeuf asiatique'
        ],
        plats: [
            'magret de canard',
            'moules frites',
            'couscous',
            'blanquette de veau',
            'côte de bœuf',
            "gigot d'agneau",
            'steak-frites',
            'bœuf bourguignon',
            'raclette',
            'tomates farcies'
        ],
        desserts: [
            'fondant au chocolat',
            'crêpes',
            'mousse au chocolat',
            'île flottante',
            'tarte aux pommes',
            'tiramisu',
            'crème brûlée',
            'profiteroles',
            'millefeuille',
            'tarte aux fraises'
        ]
    }
];
