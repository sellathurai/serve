# Requirements

- Redis
- Node >= `14.16.1`
- Yarn
- Mongodb >= `4.2.12`

# Base de données MongoDB

Les entités suivantes doivent exister :

- Une base de données `plats`
- Une collection `plats`

# Installation du projet

- Installation des modules : `yarn`
- Lancement du projet : `npm run start`

# Commandes utiles

- Build du projet : `npm run build`

# Fonctionnement

- Toutes les minutes un plat est servis automatiquement.
- Forcer l'envoi d'un plat : `http://127.0.0.1:8090/serve`

# Notes

TODO :

- [x] partie 1
- [x] partie 2 : utilidation d'un cron (toutes les minutes) par manque de temps
- [x] partie 3
- [ ] partie 4
- [ ] partie 5
- [ ] partie 6
