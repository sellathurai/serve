import fastify from "fastify";
import fastifyCron from "fastify-cron";

import { registerPlat } from "./service/registerPlat";
import servePlat from "./service/servePlat";
import { pendingPlat } from "./service/pendingPlat";

const server = fastify();

server.register(fastifyCron, {
  jobs: [
    {
      cronTime: "* * * * *",
      onTick: async (server) => {
        const plat = servePlat();
        await pendingPlat(plat);
        console.log(`Plat ${plat} envoyé !`);
      },
    },
  ],
});

server.get("/", (request: any, reply: any) => {
  Promise.all([registerPlat]);

  reply.send("Server is running fine!");
});

server.get("/serve", (request: any, reply: any) => {
  const plat = servePlat();
  try {
    pendingPlat(plat);

    reply.send(`Plat ${plat} envoyé !`);
  } catch (e) {
    console.error(e);

    reply.send(`Une erreur est survenue pendant l'envoie du plat ${plat}`);
  }
});

server.listen(8090, (err: any, address: any) => {
  if (err) {
    console.error(err);
    process.exit(1);
  }
  server.cron.startAllJobs();
  console.log(`Server listening at ${address}`);
});
