"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fastify_1 = __importDefault(require("fastify"));
var fastify_cron_1 = __importDefault(require("fastify-cron"));
var registerPlat_1 = require("./service/registerPlat");
var servePlat_1 = __importDefault(require("./service/servePlat"));
var pendingPlat_1 = require("./service/pendingPlat");
var server = fastify_1.default();
server.register(fastify_cron_1.default, {
    jobs: [
        {
            cronTime: "* * * * *",
            onTick: function (server) { return __awaiter(void 0, void 0, void 0, function () {
                var plat;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            plat = servePlat_1.default();
                            return [4 /*yield*/, pendingPlat_1.pendingPlat(plat)];
                        case 1:
                            _a.sent();
                            console.log("Plat " + plat + " envoy\u00E9 !");
                            return [2 /*return*/];
                    }
                });
            }); },
        },
    ],
});
server.get("/", function (request, reply) {
    Promise.all([registerPlat_1.registerPlat]);
    reply.send("Server is running fine!");
});
server.get("/serve", function (request, reply) {
    var plat = servePlat_1.default();
    try {
        pendingPlat_1.pendingPlat(plat);
        reply.send("Plat " + plat + " envoy\u00E9 !");
    }
    catch (e) {
        console.error(e);
        reply.send("Une erreur est survenue pendant l'envoie du plat " + plat);
    }
});
server.listen(8090, function (err, address) {
    if (err) {
        console.error(err);
        process.exit(1);
    }
    server.cron.startAllJobs();
    console.log("Server listening at " + address);
});
