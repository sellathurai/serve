"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pendingPlat = void 0;
var bullmq_1 = require("bullmq");
var constantes_1 = require("../constantes");
var queue = new bullmq_1.Queue(constantes_1.QUEUE_NAME);
var pendingPlat = function (plat) {
    console.log("Ajout du plat " + plat + " dans la queue dans le job " + constantes_1.JOB_NAME);
    return queue.add(constantes_1.JOB_NAME, plat);
};
exports.pendingPlat = pendingPlat;
