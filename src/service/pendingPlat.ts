import { Queue } from "bullmq";

import { QUEUE_NAME, JOB_NAME } from "../constantes";

const queue = new Queue(QUEUE_NAME);

export const pendingPlat = (plat: string) => {
  console.log(`Ajout du plat ${plat} dans la queue dans le job ${JOB_NAME}`);
  return queue.add(JOB_NAME, plat);
};
