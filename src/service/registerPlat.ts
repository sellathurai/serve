import { Worker } from "bullmq";
import { MongoClient } from "mongodb";

import { checkIfPlatExists, insertPlat } from "../model/plat";
import { JOB_NAME, QUEUE_NAME } from "../constantes";

const url = "mongodb://localhost:27017";
const dbName = "plats";
let db;

const client = new MongoClient(url, { useUnifiedTopology: true });

export const registerPlat = new Worker(QUEUE_NAME, async (job) => {
  if (job.name === JOB_NAME) {
    const plat = job.data;

    console.log(`Process du job ${JOB_NAME} avec la donnée : ${plat}`);

    try {
      await client.connect();
      const connection = await client.db(dbName);
      const collection = await connection.collection("plats");

      const isPlatExists = await checkIfPlatExists(collection, plat);

      if (!isPlatExists) {
        await insertPlat(collection, plat);
      }
    } catch (e) {
      console.error(e);
    }
  }
});
