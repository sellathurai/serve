import { Collection } from "mongodb";

import { plats } from "../../plats_mock";

export const checkIfPlatExists = async (
  collection: Collection,
  plat: string
): Promise<boolean> => {
  const findPlat = await collection.findOne({ plat });

  if (!findPlat) {
    console.log(`C'est un nouveau plat !`);

    return false;
  }

  console.log("Le plat est déjà enregistré !");

  return true;
};

export const insertPlat = async (collection: Collection, plat: string) => {
  console.log(`Enregistrement du plat: ${plat}`);

  return collection.insertOne({ plat });
};

export const getPlat = (): string => {
  const randIndex = Math.floor(Math.random() * plats[0].plats.length);
  return plats[0].plats[randIndex];
};
